package app.utils;

public class Constants {

    public static final String NAME_REGEX = "^[a-zA-Z]+";
    public static final String PASSWORD_REGEX = "^[a-zA-Z0-9#?!@$ %^&*-]+";
    public static final String EMAIL_REGEX = "[^@ \\t\\r\\n]+@[^@ \\t\\r\\n]+\\.[^@ \\t\\r\\n]+";
    public static final String PRICE_REGEX = "^\\d+(\\.\\d{1,2})?$";

}
